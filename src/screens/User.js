import React,{useState,useEffect} from 'react'
import axios from 'axios'


const User =() => {

  const [userDetails,setUserDetails] = useState()
  const [isLoading,setIsLoading] =useState(false)

  const userList=async()=>{
    setIsLoading(true)
    try{
        let res =await axios({url:'https://randomuser.me/api'})

        localStorage.setItem("user",
        JSON.stringify({name:`${res.data.results[0].name.title} ${res.data.results[0].name.first} ${res.data.results[0].name.last}`,email:res.data.results[0].email})
        )

        setUserDetails(res.data.results[0])
    }
    catch(err){
        console.log(err)
    }
    setIsLoading(false)
  }

  useEffect(()=>{
    userList()
  },[])

  const fetchNewUser =()=>{
    userList()
  }


  return (
    <>
        <div>
            Name : {isLoading?'Loading...':<i>{userDetails && userDetails.name.title} {userDetails && userDetails.name.first} { userDetails && userDetails.name.last}</i>}
            <br/>
            Email : {isLoading?'Loading...':<i>{userDetails && userDetails.email}</i>}
        </div>
        <br/>
        <button
            style={{
                color:'white',
                background:'#282c34',
                border:'2px solid white',
                borderRadius:'5px',
                padding:'10px',
                cursor:'pointer'
                }}
            onClick={fetchNewUser}
        >
            Refresh
        </button>
    </>
  )
}

export default User