import './App.css';
import User from './screens/User'

const App =() => {
  return (
    <div className="App-header">
        <User/>
    </div>
  );
}

export default App;
